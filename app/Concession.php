<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concession extends Model
{
    

    protected $table = 'concessions';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['name', 'event_id', 'unit_cost', 'quantity', 'sold', 'generate_ticket', 'user_id'];

    public function event() {
        return $this->belongsTo(event::class, 'event_id');
    }

}
