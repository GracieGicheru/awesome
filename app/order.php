<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table= 'orders';
    protected $fillable= ['amount', 'tickets','status','customer_id','event_id'];
}
