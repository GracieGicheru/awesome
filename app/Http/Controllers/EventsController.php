<?php

namespace App\Http\Controllers;

use App\Log;
use Illuminate\Http\Request;
use App\event;
use Illuminate\Support\Facades\Auth;
use Upload;
use Carbon\Carbon;
use App\TicketType;
use App\Concession;
use App\Promotion;
use App\Coupon;
use App\Payment;
use App\Status;
use App\Currency;
use Illuminate\Support\Facades\Validator;

class EventsController extends Controller
{
    //

    public function getEvents(){
        $currencys= Currency::all();
        $payments= Payment::all();
        $statuses= Status::all();
    	return view ('tickets.addevents')->with('currencys', 'payments', 'statuses');
    }


    public function show($id) {
        $event = event::find($id);
        $ticketTypes = TicketType::all();
        $concessions= Concession::all();
        // $concessions= Concession::where($event->id)->get();
        $promotions= Promotion::all();
        $coupons= Coupon::find('promotion_id');

        return view('tickets.viewEvent', compact('event', 'ticketTypes', 'concessions','promotions','coupons'));
    }

    public function addEvents(Request $request){
        dd($request);

        if ($request->hasFile('poster')) {
            $file = $request->file('poster');                  
            $fileName = 'poster_'.Carbon::now()->timestamp.'.'.$file->getClientOriginalExtension();
            $request->poster->storeAs('public/posters/', $fileName);
            $filepath = storage_path('app/public/posters/');
$rules=[
    	        'event_name' => 'required|max:255',
    	        'location' => 'nullable',
    	        'organizer' => 'required',
    	        'status' => 'required',
    	        'frequency' => 'required',
    	        'start_date' => 'required',
    	        'end_date' => 'required',
    	        'featured' => 'required',
    	        'currency' => 'required',
    	        'seating' => 'required',
    	        'description' => 'required',
    	        
            ];        
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->getMessagebag()->toarray();
                $array = array_values($errors);
                $msg = '';
                for ($i = 1; $i <= sizeof($array); $i++) {
                    $msg .= $array[$i - 1][0] . PHP_EOL;
                }
                return response()->json(['errors' =>  $msg]);
            }
    
            // $file = $this->fileUpload(request('poster'));
          
        	$event= new Event;
        	$event->event_name= $request->event_name;
        	$event->location= $request->location;
        	$event->organizer= $request->organizer;
        	$event->status= $request->status;
        	$event->frequency= $request->frequency;
        	$event->start_date= $request->start_date;
        	$event->end_date= $request->end_date;
        	$event->featured= $request->featured;
        	$event->currency= $request->currency;
        	$event->seating= $request->seating;
        	$event->image= $fileName;
            // $event->image= $this->uploadImage($fName);
        	$event->description= $request->description;
            $event->save();
            
            return response ()->json('ok');

            // return response ()->json(['status'=> "ok"]);
         }
        // else {
        //     return response ()->json(['error'=> "error with poster"]);
        // }    

    }

    public function viewEvents(){
        $event= event::all();
    	return view('tickets.events')->with('events', $event);
    }


      public function editEvents(Request $request){

        if ($request->hasFile('poster1')) {
            $file = $request->file('poster1');                  
            $fileName = 'poster1_'.Carbon::now()->timestamp.'.'.$file->getClientOriginalExtension();
            $request->poster1->storeAs('posters/', $fileName);
            $filepath = storage_path('app/posters/');

        $event=Event::find($request->id);
        $event->event_name= $request->input('event_name1');
        $event->location= $request->input('location1');
        $event->organizer= $request->input('organizer1');
        $event->status= $request->input('status1');
        $event->frequency= $request->input('frequency1');
        $event->start_date= $request->input('start_date1');
        $event->end_date= $request->input('end_date1');
        $event->featured= $request->input('featured');
        $event->currency= $request->input('currency');
        $event->seating= $request->input('seating1');
        $event->image= $fileName;
        $event->description= $request->input('description1');
        $event->save();

        return response ()->json([
            'status'=> "ok"
        ]);
            }

    

    }


       public function deleteEvent(Request $request){
        try{

            event::find($request->delId)->delete();
            return response(['status'=> 'ok']);
        }catch(Exception $e){
            return response (['status'=> 'error']);

        }
    }

      






 


}
