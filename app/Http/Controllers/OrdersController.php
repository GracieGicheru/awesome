<?php

namespace App\Http\Controllers;

use App\Concession;
use App\Customer;
use App\order;
use App\Ticket;
use App\TicketType;
use App\event;
use App\concessionSale;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    //

    public function createOrder()
    {
//        dd(\request()->all());
        //create or update the customer
        $customer = Customer::where('email',\request()->email)->first();
        if (!$customer)
        {
            $customer =  new Customer;
            $customer->name = \request()->name;
            $customer->email = \request()->email;
            $customer->phone = \request()->phone;
            $customer->save();
        }

        //create the order
        $order = new order();
        $order->customer_id = $customer->id;
        $order->event_id = \request()->event_id;
        $order->amount = \request()->total_cost;
        $order->tickets = 0;
        $order->save();


        $details = \request()->ticket;

        //create the tickets
        foreach($details as $id => $data) {

            if ($data['ticket_number'] > 0)
            {

                $ticket_type = TicketType::findOrFail($data['ticket_id']);

                $ticket = new Ticket();
                $ticket->customer_id = $customer->id;
                $ticket->order_id = $order->id;
                $ticket->event_id = \request()->event_id;
                $ticket->ticket_type_id = $data['ticket_id'];
                $ticket->tickets = $ticket_type->name;
                $ticket->ticket_number = $data['ticket_number'];
                $ticket->total_cost = $data['ticket_cost']  * $data['ticket_number'];
                $ticket->save();


                //update the order tickets
                $order->tickets += $data['ticket_number'];
                $order->save();
            }


        }

        session()->flash('success','Order saved');
        return redirect()->to("add-concessions/$order->id");

    }
  

    public function viewConcession($order_id)
    {
        $order =  order::findOrFail($order_id);
        $events = event::findOrFail($order->event_id);
        $tickets= Ticket::where('order_id', $order_id)->get();
        $concessions= Concession::all();
        
        // $tickets = Ticket::where('order_id',$order_id)->get();

        return view('tickets.front-end-concession',compact(['order', 'events', 'tickets', 'concessions']));
    }
    public function addConcession(Request $request){
    
        $details = \request()->ticket;

        $order =  order::findOrFail($request->order_id);
        foreach($details as $id => $data) {

            $ticket = Ticket::where('id',$id)->first();
            $customer = Customer::where('email',\request()->email)->first();
            if (!$customer)
            {
                $customer =  new Customer;
                $customer->name = $data['name'];
                $customer->email = $data['email'];
                $customer->phone = $data['phone'];
                $customer->save();
            }

            //find the concession
            $concession = Concession::findOrFail($data['add_concession']);
            $cost = $concession->unit_cost * $data['quantity'];

            $concession_sale= new concessionSale;
            $concession_sale->name= $data['name'];
            $concession_sale->email= $data['email'];
            $concession_sale->concession= $concession->concession_name;
            $concession_sale->quantity= $data['quantity'];
            $concession_sale->cost= $cost;
            $concession_sale->ticket_id= $id;
            $concession_sale->order_id= \request()->order_id;
            $concession_sale->event_id= \request()->event_id;
            $concession_sale->customer_id = $customer->id;
            $concession_sale->save();


            $ticket->concession_cost = $cost;
            $ticket->save();

            $order->amount += $cost;
            $order->save();
  

        }
        return redirect()->to("view-order/$order->id");
    
    }

    public function viewOrder($order_id){

        $order =  order::findOrFail($order_id);
        $events = event::findOrFail($order->event_id);
        $tickets= Ticket::where('order_id', $order_id)->get();
        $concessions= Concession::all();

        return view('view_order',compact(['order', 'events', 'tickets', 'concessions']));

    }

    public function payForOrder(Request $request)
    {

        $order_id = $request->order_id;
        $phone = $request->phone;
        $order =  order::findOrFail($order_id);
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'account' =>'ET'.$order_id,
            'group' => 'digital',
            'amount' => round($order->amount,0),
            'msisdn' => $this->formatPhone($phone),
            'description' => "payment for order-".$order_id
        );


        $data_string = $curl_post_data;
        $url = 'https://trans.standardmedia.co.ke/api/checkout';
        $res = $this->doCurl($url,$data_string,'POST',$headers = null);


        // dd($res);

    }

    protected function doCurl($url,$data,$method='POST',$header = null){
        if (!$header) {
            $header = array(
                'Accept: application/json',
                'Accept-Language: en_US',
            );
        }
        $curl = \curl_init($url);
        \curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_POST, true);

        \curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        \ curl_setopt($curl, CURLOPT_HEADER, 0);
        \curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        \curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $content = \curl_exec($curl);
        $status = \curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $json_response = null;
        if ($status == 200 || $status == 201) {
            $json_response = json_decode($content);
            return $json_response;
        } else {
            throw new \Exception($content);
        }
    }

    public function formatPhone($phone){
        $phone = 'hfhsgdgs'.$phone;
        $phone = str_replace('hfhsgdgs0','',$phone);
        $phone = str_replace('hfhsgdgs','',$phone);
        $phone = str_replace('+','',$phone);
        if(strlen($phone) == 9){
            $phone = '254'.$phone;
        }
        return $phone;
    }
    public function qrGenerator(){
        $file= public_path('qr.png');
        return \QRCode::text('QR Code Generator for Laravel!')->setOutfile($file)->png();  
    }

    public function viewmail(){
        return view('tickets.send_email');
    }

    public function sendEmail()
    {
        $payload = [
            "email" => 'gracegicheru57@gmail.com',
            "subject" => 'test message',
            "message" => 'Hello there'
        ];
        $apiurl = 'https://mail.standarddigitalworld.co.ke/api/transactionalMail';
        $client = new Client();
        $response = $client->request('POST', $apiurl, [
            'body' => json_encode($payload),
            'headers' => ["appkey" => "QW1UTjBXZzAxSVBrSTJLbmlQVlk0SDBNOWJJZ095S2VqTDM2R2RHbG1JdjZXSVFjMG1hWUxvWEhmY2hB5eafd4feeb556"]
        ]);
        return response()->json($response->getBody());
    }

    
}
