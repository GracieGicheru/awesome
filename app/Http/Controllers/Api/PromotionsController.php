<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\event;
use App\Promotion;
use App\Coupon;

class PromotionsController extends Controller
{
    public function addPromotions(Request $request){
        $validatedData = $request->validate([
            'promotion_name'=>'required',
            'coupons'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'type'=>'required',
            'amount'=>'required'
        ]);


        $promotion= new Promotion;
        $promotion->promotion_name= $validatedData['promotion_name'];
        $promotion->coupons= $validatedData['coupons'];
        $promotion->start_date= $validatedData['start_date'];
        $promotion->end_date= $validatedData['end_date'];
        $promotion->amount= $validatedData['amount'];
        $promotion->promotion_type= $validatedData['type'];
        $
        $promotion->save();

        $couponCodes = $this->couponCodes('SG-', $promotion->coupons);
        foreach ($couponCodes as $code) {
            $coupon = new Coupon(['promotion_id' => $promotion->id, 'code' => $code]);
            $coupon->save();
        }

        
        $promotions= Promotion::all();
        $coupons= Coupon::find('promotion_id');
        return view('tickets.promotion_datatable', compact('promotions', 'coupons'));


    }

    function couponCodes($prefix, $quantity) {
        $prefix = trim($prefix) == "" ? 'SG-' : trim($prefix).'-';

        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $codes = [];

        for ($i=0; $i<$quantity; $i++) {
            $code = $prefix.substr(str_shuffle($str_result), 0, 4);
            array_push($codes, $code);
        }
        return $codes;
    }

    public function editPromotion(Request $request){
        $promotion= Promotion::find($request->id);
        $promotion->promotion_name= $request->input('promotion_name1');
        $promotion->coupons= $request->input('coupons1');
        $promotion->promotion_type= $request->input('type1');
        $promotion->start_date= $request->input('start_date1');
        $promotion->end_date= $request->input('end_date1');
        $promotion->amount= $request->input('amount1');
        $promotion->save();
        $promotions= Promotion::all();
        return view('tickets.promotion_datatable', compact('promotions'));

    }

    public function viewCoupon(){
        $coupons= Coupon::find('promotion_id' ==1);
    }

    


}
