<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\event;
use Upload;
use Carbon\Carbon;
use App\TicketType;
use App\Concession;
use App\Promotion;
use App\Coupon;

class EventsController extends Controller
{
    //

    public function getEvents(){
    	return view ('tickets.addevents');
    }


    public function show($id) {
        $event = event::find($id);
        $ticketTypes = TicketType::all();
        $concessions= Concession::all();
        $promotions= Promotion::all();
        $coupons= Coupon::find('promotion_id');

        return view('tickets.viewEvent', compact('event', 'ticketTypes', 'concessions','promotions','coupons'));
    }

    public function addEvents(Request $request){
        // dd($request);

        if ($request->hasFile('poster')) {
            $file = $request->file('poster');                  
            $fileName = 'poster_'.Carbon::now()->timestamp.'.'.$file->getClientOriginalExtension();
            $request->poster->storeAs('public/posters/', $fileName);
            $filepath = storage_path('app/public/posters/');


    	 	$validatedData = $request->validate([
    	        'event_name' => 'required|max:255',
    	        'location' => 'nullable',
    	        'organizer' => 'required',
    	        'status' => 'required',
    	        'frequency' => 'required',
    	        'start_date' => 'required',
    	        'end_date' => 'required',
    	        'featured' => 'required',
    	        'currency' => 'required',
    	        'seating' => 'required',
    	        'description' => 'required',
    	        
    	    ]);
            // $file = $this->fileUpload(request('poster'));
          
        	$event= new Event;
        	$event->event_name= $validatedData['event_name'];
        	$event->location= $validatedData['location'];
        	$event->organizer= $validatedData['organizer'];
        	$event->status= $validatedData['status'];
        	$event->frequency= $validatedData['frequency'];
        	$event->start_date= $validatedData['start_date'];
        	$event->end_date= $validatedData['end_date'];
        	$event->featured= $validatedData['featured'];
        	$event->currency= $validatedData['currency'];
        	$event->seating= $validatedData['seating'];
        	$event->image= $fileName;
            // $event->image= $this->uploadImage($fName);
        	$event->description= $validatedData['description'];
            $event->save();
            

            return redirect('viewEvents');

            // return response ()->json(['status'=> "ok"]);
         }
        // else {
        //     return response ()->json(['error'=> "error with poster"]);
        // }    

    }

    public function viewEvents(){
        $event= event::all();
    	return view('tickets.events')->with('events', $event);
    }


      public function editEvents(Request $request){

        if ($request->hasFile('poster1')) {
            $file = $request->file('poster1');                  
            $fileName = 'poster1_'.Carbon::now()->timestamp.'.'.$file->getClientOriginalExtension();
            $request->poster1->storeAs('posters/', $fileName);
            $filepath = storage_path('app/posters/');

        $event=Event::find($request->id);
        $event->event_name= $request->input('event_name1');
        $event->location= $request->input('location1');
        $event->organizer= $request->input('organizer1');
        $event->status= $request->input('status1');
        $event->frequency= $request->input('frequency1');
        $event->start_date= $request->input('start_date1');
        $event->end_date= $request->input('end_date1');
        $event->featured= $request->input('featured');
        $event->currency= $request->input('currency');
        $event->seating= $request->input('seating1');
        $event->image= $fileName;
        $event->description= $request->input('description1');
        $event->save();

        return response ()->json([
            'status'=> "ok"
        ]);
            }

    

    }


       public function deleteEvent(Request $request){
        try{

            event::find($request->delId)->delete();
            return response(['status'=> 'ok']);
        }catch(Exception $e){
            return response (['status'=> 'error']);

        }
    }

      public function addTicketType(Request $request){

        $validatedData= $request->validate([
            'name'=> 'required',
            'unit_price'=>'required',
            'tickets_available'=>'required',
            'group_ticket'=>'required',
            'ticket_sales_start'=>'required',
            'ticket_sales_end'=>'required',
            
        ]);

        $ticket_type=new TicketType;
        $ticket_type->name= $validatedData['name'];
        $ticket_type->unit_price= $validatedData['unit_price'];
        $ticket_type->tickets_available= $validatedData['tickets_available'];
        $ticket_type->group_ticket= $validatedData['group_ticket'];
        $ticket_type->ticket_sales_start= $validatedData['ticket_sales_start'];
        $ticket_type->ticket_sales_end= $validatedData['ticket_sales_end'];
        $ticket_type->event_id= $request->input('event_id');

        $ticket_type->save();

        $ticketTypes= TicketType::all();
        
        return view('tickets.ticket_type_table', compact('ticketTypes'));
        
    }
    public function editTicketType(Request $request){
        $ticketType=TicketType::find($request->id);
        $ticketType->name= $request->input('ticket_name1');
        $ticketType->unit_price= $request->input('unit_price1');
        $ticketType->tickets_available= $request->input('tickets_available1');
        $ticketType->group_ticket= $request->input('group_ticket1');
        $ticketType->ticket_sales_start= $request->input('tickets_sales_start1');
        $ticketType->ticket_sales_end= $request->input('tickets_sales_end1');

        $ticketType->save();

        $ticketTypes= TicketType::all();
        
        return view('tickets.ticket_type_table', compact('ticketTypes'));

    }


    public function addConcession(Request $request){

        $validatedData= $request->validate([
            'concession_name'=> 'required',
            'unit_cost'=>'required',
            'quantity'=>'required',
            'individual_ticket'=>'required',
            
        ]);

        $concession=new Concession;
        $concession->concession_name= $validatedData['concession_name'];
        $concession->unit_cost= $validatedData['unit_cost'];
        $concession->quantity= $validatedData['quantity'];
        $concession->individual_ticket= $validatedData['individual_ticket'];

        $concession->save();

        $concessions= Concession::all();
        
        return view('tickets.concession_datatable', compact('concessions'));
        
    }

    public function editConcession(Request $request){

        $concession=Concession::find($request->id);
        // dd($concession);
        $concession->concession_name= $request->input('concession_name1');
        $concession->unit_cost= $request->input('unit_cost1');
        $concession->quantity= $request->input('quantity1');
        $concession->individual_ticket= $request->input('individual_ticket1');
       
        $concession->save();
        $concessions= Concession::all();
  
        return view('tickets.concession_datatable', compact('concessions'));
    
    }



 


}
