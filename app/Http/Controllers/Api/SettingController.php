<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Status;
use App\Payment;

class SettingController extends Controller
{
    //

    public function Currency(){

    	$currency= Currency::all();
    	return view('tickets.currency')-> with('currencies', $currency);
    }
    public function AddCurrency(Request $request){

    	$validatedData= $request->validate([
            'name'=> 'required',
            'code'=>'required',
            
        ]);

        $currency=new Currency;
        $currency->name= $validatedData['name'];
        $currency->code= $validatedData['code'];

        $currency->save();

        return response ()->json([
            'status'=> "ok"
        ]);
    	
    }

    public function EditCurrency(Request $request){

    	$currency=Currency::find($request->id);
        $currency->name= $request->input('name1');
        $currency->code= $request->input('code1');
       
        $currency->save();
  
        return response()->json([
            'status'=>'ok'
        ]);
    }

    public function deleteCurrency(Request $request){

    	   try{

            Currency::find($request->delId)->delete();
            return response(['status'=> 'ok']);
        }catch(Exception $e){
            return response (['status'=> 'error']);

        }

    }

    public function Status(){
    	$status= Status::all();

    	return view('tickets.status')->with('statuses', $status);
    }

    public function addStatus(Request $request){

    	$validatedData= $request->validate([
            'status'=> 'required',
            
        ]);

        $status=new Status;
        $status->status= $validatedData['status'];
        

        $status->save();

        return response ()->json([
            'status'=> "ok"
        ]);

    }

     public function EditStatus(Request $request){

    	$status=Status::find($request->id);
        $status->status= $request->input('status1');
       
        $status->save();
  
        return response()->json([
            'status'=>'ok'
        ]);
    }

     public function deleteStatus(Request $request){

    	   try{

            Status::find($request->delId)->delete();
            return response(['status'=> 'ok']);
        }catch(Exception $e){
            return response (['status'=> 'error']);

        }

    }

    public function Payment(){
    	$payment= Payment::all();

    	return view('tickets.payments')->with('payments', $payment);
    }

    public function addPayment(Request $request){

    	$validatedData= $request->validate([
            'payment'=> 'required',
            
        ]);

        $payment=new Payment;
        $payment->payment= $validatedData['payment'];
        

        $payment->save();

        return response ()->json([
            'status'=> "ok"
        ]);

    }

      public function editPayment(Request $request){

    	$payment=Payment::find($request->id);
        $payment->payment= $request->input('payment1');
       
        $payment->save();
  
        return response()->json([
            'status'=>'ok'
        ]);
    }

      public function deletePayment(Request $request){

    	   try{

            Payment::find($request->delId)->delete();
            return response(['status'=> 'ok']);
        }catch(Exception $e){
            return response (['status'=> 'error']);

        }

    }




}
