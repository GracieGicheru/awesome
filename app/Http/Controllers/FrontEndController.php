<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\event;
use App\Http\Controllers\Controller;
use App\TicketType;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    //

    public function getEvents()
    {
        $events= event::all();

        return json_encode($events);
    }

    public function viewSingleEvent($id)
    {
        $event= event::findOrFail(decrypt($id));

        return view('single_event',compact(['event']));
    }

    public function getTicketTypePrice($id)
    {
        $ticket_type = TicketType::findOrFail($id);

        return $ticket_type->unit_price;
    }

    public function validateCoupon(Request $request,$id)
    {
//        dd($request->coupon_code);
        //get the event and promotion
       $coupon = Coupon::where('code',$request->coupon_code)->where('event_id',$id)->first();
       if($coupon)
       {
           $res = 0;
           return $res;
       }
       else
       {
           $res = 1;
           return $res;
       }

    }
}
