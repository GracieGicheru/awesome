<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TicketType;
use App\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TicketTypeController extends Controller
{
    public function addTicketType(Request $request){


        $rules = array(
            'name' => 'required',
            'unit_price' => 'required',
            'group_ticket' => 'required',
            'tickets_available' => 'required',
            'ticket_sales_start' => 'required',
            'ticket_sales_end' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->getMessagebag()->toarray();
            $array = array_values($errors);
            $msg = '';
            for ($i = 1; $i <= sizeof($array); $i++) {
                $msg .= $array[$i - 1][0] . PHP_EOL;
            }
            return response()->json(['errors' =>  $msg]);
        }


   

        $ticket_type=new TicketType;
        $ticket_type->name= $request->input('name');
        $ticket_type->unit_price= $request->input('unit_price');
        $ticket_type->tickets_available= $request->input('tickets_available');
        $ticket_type->group_ticket= $request->input('group_ticket');
        $ticket_type->ticket_sales_start= $request->input('ticket_sales_start');
        $ticket_type->ticket_sales_end= $request->input('ticket_sales_end');
        $ticket_type->event_id= $request->input('event_id');

        $ticket_type->save();

        //create new log

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->slug = 'created_ticket_type';
        $log->description = 'New Event Ticket Type has been created';
        $log->save();

        $ticketTypes= TicketType::all();
    

        
        return view('tickets.ticket_type_table', compact('ticketTypes'));
        
    }
    public function editTicketType(Request $request){

        $rules = array(
            'ticket_name1' => 'required',
            'unit_price1' => 'required',
            'group_ticket1' => 'required',
            'tickets_available1' => 'required',
            'tickets_sales_start1' => 'required',
            'tickets_sales_end1' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->getMessagebag()->toarray();
            $array = array_values($errors);
            $msg = '';
            for ($i = 1; $i <= sizeof($array); $i++) {
                $msg .= $array[$i - 1][0] . PHP_EOL;
            }
            return response()->json(['errors' =>  $msg]);
        }
        $ticketType=TicketType::find($request->id);
        $ticketType->name= $request->input('ticket_name1');
        $ticketType->unit_price= $request->input('unit_price1');
        $ticketType->tickets_available= $request->input('tickets_available1');
        $ticketType->group_ticket= $request->input('group_ticket1');
        $ticketType->ticket_sales_start= $request->input('tickets_sales_start1');
        $ticketType->ticket_sales_end= $request->input('tickets_sales_end1');

        $ticketType->save();

        $ticketTypes= TicketType::all();
        
        return view('tickets.ticket_type_table', compact('ticketTypes'));

    }

    public function destroyTicket($id)
    {
        
      TicketType::where('id',$id)->delete();
      

      return back();
    }

}
