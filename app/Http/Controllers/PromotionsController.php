<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\event;
use App\Promotion;
use App\Coupon;
use App\Log;
use App\Concession;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PromotionsController extends Controller
{
    public function addPromotions(Request $request){

        $rules = array(
            'promotion_name' => 'required',
            'coupons' => 'required',
            'type' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'amount' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->getMessagebag()->toarray();
            $array = array_values($errors);
            $msg = '';
            for ($i = 1; $i <= sizeof($array); $i++) {
                $msg .= $array[$i - 1][0] . PHP_EOL;
            }
            return response()->json(['errors' =>  $msg]);
        }
      


        $promotion= new Promotion;
        $promotion->promotion_name= $request->input('promotion_name');
        $promotion->coupons= $request->input('coupons');
        $promotion->start_date= $request->input('start_date');
        $promotion->end_date= $request->input('end_date');
        $promotion->amount= $request->input('amount');
        $promotion->promotion_type= $request->input('type');
        $promotion->event_id= $request->input('event_id');
        $promotion->save();
        $couponCodes = $this->couponCodes('SG-', $promotion->coupons);
        foreach ($couponCodes as $code) {
            $coupon = new Coupon(['promotion_id' => $promotion->id, 'code' => $code]);
            $coupon->event_id= $request->input('event_id');
            $coupon->save();
        }

        
        $promotions= Promotion::all();
        $coupons= Coupon::find('promotion_id');
        return view('tickets.promotion_datatable', compact('promotions', 'coupons'));


    }

    function couponCodes($prefix, $quantity) {
        $prefix = trim($prefix) == "" ? 'SG-' : trim($prefix).'-';

        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $codes = [];

        for ($i=0; $i<$quantity; $i++) {
            $code = $prefix.substr(str_shuffle($str_result), 0, 4);
            array_push($codes, $code);
        }
        return $codes;
    }

    public function editPromotion(Request $request){

        $rules = array(
            'promotion_name1' => 'required',
            'coupons1' => 'required',
            'type1' => 'required',
            'start_date1' => 'required',
            'end_date1' => 'required',
            'amount1' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->getMessagebag()->toarray();
            $array = array_values($errors);
            $msg = '';
            for ($i = 1; $i <= sizeof($array); $i++) {
                $msg .= $array[$i - 1][0] . PHP_EOL;
            }
            return response()->json(['errors' =>  $msg]);
        }
        $promotion= Promotion::find($request->id);
        $promotion->promotion_name= $request->input('promotion_name1');
        $promotion->coupons= $request->input('coupons1');
        $promotion->promotion_type= $request->input('type1');
        $promotion->start_date= $request->input('start_date1');
        $promotion->end_date= $request->input('end_date1');
        $promotion->amount= $request->input('amount1');
        $promotion->save();

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->slug = 'Edited_promotion';
        $log->description = 'The Promotion has been Edited';
        $log->save();
        $promotions= Promotion::all();
      
        return view('tickets.promotion_datatable', compact('promotions'));

    }
    public function destroyPromotion(Request $request,$id)
    {
      Promotion::where('id',$id)->delete();
    //   return back();
      $log = new Log;
      $log->user_id = Auth::user()->id;
      $log->slug = 'Deleted_promotion';
      $log->description = 'The Promotion has been deleted';
      $log->save();
    $promotions= Promotion::all();
    return view('tickets.promotion_datatable', compact('promotions'));
    }

    public function viewCoupon($id){
        $promotion= Promotion::findOrFail($id);
        $coupons= Coupon::where('promotion_id',$promotion->id)->get();

        $str ='';
        foreach($coupons as $coupon){
            $str.='<span>'.$coupon->code. ','. '</span>';

        }
        

        return $str;
    }


    public function destroyConcession($id)
    {
              
      Concession::where('id',$id)->delete();
      return back();
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->slug = 'Deleted_promotion';
        $log->description = 'The Promotion has been deleted';
        $log->save();
    }

    


}
