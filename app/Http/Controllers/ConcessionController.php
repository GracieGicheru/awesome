<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Concession;
use App\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ConcessionController extends Controller
{
    //
    public function addConcession(Request $request){

        $rules = array(
            'concession_name' => 'required',
            'unit_cost' => 'required',
            'quantity' => 'required',
            'individual_ticket' => 'required',
           
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->getMessagebag()->toarray();
            $array = array_values($errors);
            $msg = '';
            for ($i = 1; $i <= sizeof($array); $i++) {
                $msg .= $array[$i - 1][0] . PHP_EOL;
            }
            return response()->json(['errors' =>  $msg]);
        }

        $concession=new Concession;
        $concession->concession_name= $request->input('concession_name');
        $concession->unit_cost= $request->input('unit_cost');
        $concession->quantity= $request->input('quantity');
        $concession->individual_ticket= $request->input('individual_ticket');
        $concession->event_id= $request->input('event_id');

        $concession->save();

        // $log = new Log;
        // $log->user_id = Auth::user()->id;
        // $log->slug = 'created_concession';
        // $log->description = 'New Concession has been created';
        // $log->save();

        $concessions= Concession::all();
        // $concessions= Concession::where('event_id', 3)->get();
        // dd($concessions);
        
        return view('tickets.concession_datatable', compact('concessions'));
        
    }

    public function editConcession(Request $request){

        $rules = array(
            'concession_name1' => 'required',
            'unit_cost1' => 'required',
            'quantity1' => 'required',
            'individual_ticket1' => 'required',
           
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->getMessagebag()->toarray();
            $array = array_values($errors);
            $msg = '';
            for ($i = 1; $i <= sizeof($array); $i++) {
                $msg .= $array[$i - 1][0] . PHP_EOL;
            }
            return response()->json(['errors' =>  $msg]);
        }

        $concession=Concession::find($request->id);
        // dd($concession);
        $concession->concession_name= $request->input('concession_name1');
        $concession->unit_cost= $request->input('unit_cost1');
        $concession->quantity= $request->input('quantity1');
        $concession->individual_ticket= $request->input('individual_ticket1');
       
        $concession->save();
        $concessions= Concession::all();
  
        return view('tickets.concession_datatable', compact('concessions'));
    
    }

    public function destroyConcession($id)
    {
        
      Concession::where('id',$id)->delete();
      return back();
    }
}
