<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'promotions';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = [
        'promotion_name', 'start_date', 'end_date',  'promotion_type', 'coupons', 'amount'
    ];

    public function coupons()
    {
        return $this->hasMany(Coupon::class, 'promotion_id');
    }

    public function event()
    {
        return $this->belongsTo(event::class, 'event_id');
    }
}
