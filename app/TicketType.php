<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketType extends Model
{
    
    protected $table= 'ticket_types';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable=['ticket_name', 'unit_price', 'tickets_available', 'group_ticket', 'ticket_sales_start','ticket_sales_end'] ;

    public function event()
    {
        return $this->belongsTo(event::class, 'event_id');
    }
}
