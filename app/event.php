<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    //

    protected $table = 'events';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['event_name', 'location', 'description', 'organizer', 'status', 'frequency', 'featured', 'currency', 'seating','image', 'start_date','end_date'];

public function concessions()
{
    return $this->hasMany('Concession::class');
}

public function ticketType()
{
    return $this->hasMany(TicketType::class,'event_id');
}
public function promotion()
{
    return $this->hasMany(Promotion::class, 'event_id');
}
}