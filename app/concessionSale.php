<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class concessionSale extends Model
{
    protected $table= 'concession_sales';
    protected $fillable= ['quantity', 'cost'];
}
