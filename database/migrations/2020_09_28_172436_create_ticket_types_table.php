<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('unit_price');
            // $table->unsignedBigInteger('status_id');
            // $table->foreign('status_id')->references('id')->on('statuses');
            $table->bigInteger('tickets_available');
            $table->bigInteger('sold')->default(0);
            $table->bigInteger('sales')->default(0);
            $table->string('group_ticket');
            // $table->integer('number_of_people');
            $table->dateTime('ticket_sales_start');
            $table->dateTime('ticket_sales_end');
            $table->foreignId('event_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_types');
    }
}
