<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('event_name');
            $table->string('location')->nullable();
            $table->longText('description');
            $table->string('frequency');
            $table->string('organizer');
            $table->string('status');
            $table->string('currency');
            $table->string('featured');
            $table->string('image')->nullable();
            $table->string('start_date');
            $table->string('end_date');
            $table->string('seating')->default('NO');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
