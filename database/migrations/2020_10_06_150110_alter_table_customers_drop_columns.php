<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCustomersDropColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            //
            $table->dropColumn('ticket_number');
            $table->dropColumn('order_id');
            $table->dropColumn('ticket_type_id');
            $table->dropColumn('seat');
            $table->dropColumn('path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            //
            $table->string('ticket_number')->nullable();
            $table->string('order_id')->nullable();
            $table->string('ticket_type_id')->nullable();
            $table->string('seat')->nullable();
            $table->string('path')->nullable();
        });
    }
}
