<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('order_id')->nullable();
            $table->string('ticket_number')->nullable();
            $table->foreignId('customer_id')->nullable();
            $table->foreignId('event_id')->nullable();
            $table->string('tickets')->nullable();
            $table->string('total_cost')->nullable();
            $table->string('concession_cost')->nullable();
            $table->string('balance')->nullable();
            $table->string('discount')->nullable();
            $table->foreignId('status_id')->nullable();
            $table->string('ip')->nullable();
            $table->bigInteger('coupon_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
