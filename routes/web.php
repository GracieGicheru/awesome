<?php

use App\event;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $events= event::all();
    return view('welcome')->with('events', $events);

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('view-single-event/{id}', 'FrontEndController@viewSingleEvent');
Route::get('ticket_type_cost/{id}', 'FrontEndController@getTicketTypePrice');
Route::get('coupon-validate/{id}', 'FrontEndController@validateCoupon');
Route::post('make-order/', 'OrdersController@createOrder');
Route::get('add-concessions/{id}', 'OrdersController@viewConcession');
Route::post('add-concession', 'OrdersController@addConcession');
Route::get('view-order/{id}', 'OrdersController@viewOrder');
Route::post('pay-order', 'OrdersController@payForOrder');
Route::get('qr-code', 'OrdersController@qrGenerator');
Route::get('send-email', 'OrdersController@viewmail');
Route::get('email-send', 'OrdersController@sendEmail');




Route::get('/events', 'EventsController@getEvents');
Route::post('events/addevents', 'EventsController@addEvents')->name('addevents');
Route::get('/viewEvents', 'EventsController@viewEvents');
Route::post('/deleteEvents', 'EventsController@deleteEvent');
Route::post('/editEvents', 'EventsController@editEvents');
Route::get('/event/{id}', 'EventsController@show')->name('show_event');

Route::post('/addTicketType', 'TicketTypeController@addTicketType');
Route::post('/edit/ticket/type','TicketTypeController@editTicketType');
Route::delete('ticket/{id}','TicketTypeController@destroyTicket')->name('ticket.destroy');

Route::post('/addConcession','ConcessionController@addConcession' );
Route::post('/editConcession','ConcessionController@editConcession' );
Route::delete('concession/{id}','ConcessionController@destroyConcession')->name('concession.destroy');



Route::get('/currency', 'SettingController@Currency');
Route::post('/addCurrency', 'SettingController@AddCurrency');
Route::get('/status', 'SettingController@Status');
Route::post('/addStatus', 'SettingController@addStatus');
Route::get('/payments', 'SettingController@Payment');
Route::post('/addPayment', 'SettingController@addPayment');
Route::post('/editCurrency', 'SettingController@EditCurrency');
Route::post('/deleteCurrency', 'SettingController@deleteCurrency');
Route::post('/editStatus', 'SettingController@EditStatus');
Route::post('/deleteStatus', 'SettingController@deleteStatus');
Route::post('/editPayment', 'SettingController@editPayment');
Route::post('/deletePayment', 'SettingController@deletePayment');

Route::post('/add/promotion', 'PromotionsController@addPromotions');
Route::post('/edit/promotion', 'PromotionsController@editPromotion');
Route::get('/coupon/{id}', 'PromotionsController@viewCoupon');
Route::delete('promotion/{id}','PromotionsController@destroyPromotion')->name('promotions.destroy');




// Route::get('/getEvent', 'EventsController@SingleView');








