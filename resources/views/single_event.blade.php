@extends('layouts.app')

@section('content')
    <style>
    </style>
    <div class="container" style="margin-top: 50px;  border: 1px solid blue;">
        <h1 class="text-center">{{$event->event_name}} Details</h1>
        <div class="row justify-content-center">
            <div class="row col-md-12">
                <div class="col-md-6" >
                    <img class="card-img-top" src="{{ url('storage/posters/'.$event->image) }}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"> {{$event->event_name}}</h5>
                        <p class="m-0">{{$event->location}}</p>
                        <p class="card-text m-0">{{\Carbon\Carbon::parse($event->start_date)->toDayDateTimeString()}}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-body">
                        <h5 class="card-title"> {{$event->name}}</h5>
                        <p class="m-0">{{$event->location}}</p>
                    </div>
                    <form action="{{url('make-order')}}" method="post">
                        @csrf
                        <div class="form-group row col-md-12">
                            <div class="col-md-4">
                                <label for="staticEmail" class="col-form-label">Type</label>
                            </div>
                            <div class="col-md-4">
                                <label for="staticEmail" class="col-form-label">Number</label>
                            </div>
                            <div class="col-md-4">
                                <label for="staticEmail" class="col-form-label">Unit Price</label>
                            </div>
                        </div>
                        @foreach($event->ticketType()->get() as $ticket_type)
                        <div class="form-group row col-md-12">
                            <div class="col-md-4">
                                <p>{{$ticket_type->name}}</p>
                                <input type="hidden" name="ticket[{{$ticket_type->id}}][ticket_id]" readonly class="form-control" value="{{$ticket_type->id}}">
                            </div>
                            <div class="col-md-4">
                                <input type="number" value="0" max="{{$ticket_type->tickets_available}}" min="0" class="form-control event_tickets" onchange="calculateTotal()" name="ticket[{{$ticket_type->id}}][ticket_number]" id="{{$ticket_type->id}}">
                            </div>
                            <div class="col-md-4">
                                <p>KES:{{$ticket_type->unit_price}} </p>
                                <input type="hidden" readonly name="ticket[{{$ticket_type->id}}][ticket_cost]" value="{{$ticket_type->unit_price}}" class="form-control">
                            </div>
                        </div>
                        @endforeach
                        <div class="form-group row col-md-12">
                            <div class="col-md-4">
                                <label for="coupon" class="col-form-label">Coupon Code</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text"  name="coupon_code" onkeyup="validateCoupon()" class="form-control">
                                <span id="anchors_search_res" style="display:none"></span>
                            </div>
                            <div class="col-md-4">
                                <p id="discount">Discount</p>
                                <input type="hidden"  name="discount" class="form-control">

                            </div>
                        </div>
                        <div class="form-group row col-md-12">
                            <div class="col-md-4">
                                <label for="staticEmail" class="col-form-label">Total</label>
                            </div>
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">
                                <p id="total_cost"></p>
                                <input type="hidden" id="cost_total" name="total_cost" class="form-control">
                                <input type="hidden"  name="event_id" value="{{$event->id}}" class="form-control">
                            </div>
                        </div>
                        <h5 class="text-center">Buyers Information</h5>
                        <div class="form-group">
                            <div class="col-md-10">
                                <label for="staticEmail" class="col-form-label">Your Name</label>
                                <input type="text"  name="name" class="form-control">
                            </div>

                            <div class="col-md-10">
                                <label for="staticEmail" class="col-form-label">Your Email</label>
                                <input type="text"  name="email" class="form-control">
                            </div>
                            <div class="col-md-10">
                                <label for="staticEmail" class="col-form-label">Your Phone</label>
                                <input type="text"  name="phone" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary float-right mb-3"><i class="fa fa-arrow-right"></i>Order Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        function calculateTotal()
        {
            window.amount = 0;

            $('.event_tickets').each(function(){
                var ticket_type_id = $(this)[0].id;
                var url = '{{url('ticket_type_cost')}}';
                $.get(url+'/'+ticket_type_id,function(res){
                    var tickets = parseInt($('#'+ticket_type_id).val());

                    window.amount += parseInt(tickets) * parseInt(res);
                    console.log(window.amount)
                    $("#total_cost").html('Amount: '+ window.amount);
                    $("#cost_total").val(window.amount);
                });

            });


        }

        function validateCoupon(){
            let event_id = $("input[name='event_id']").val();
            let coupon_code =  $("input[name='coupon_code']").val();
            anchortext =  coupon_code.trim();
            let len = anchortext.length;
            if(len > 0){
                let res_opts = ['This is Valid','  This Coupon is Invalid']
                let res_opts_class = ['success','danger']
                let search_url = "{{ url('coupon-validate') }}"
                $.get(search_url+'/'+event_id+'?coupon_code='+coupon_code,function(res){
                    if (res > 0)
                    {
                        $("#anchors_search_res").html("<small class='font-weight-bold text-"+res_opts_class[1]+"'>"+res_opts[1]+"</small>")
                        $("#anchors_search_res").show()
                    }
                    else{
                        $("#anchors_search_res").html("<small class='font-weight-bold text-"+res_opts_class[res]+"'>"+res_opts[res]+"</small>")
                        $("#anchors_search_res").show()
                    }
                })
            }else{
                $("#anchors_search_res").html('')
                $("#anchors_search_res").hide()
            }

        }
    </script>
@endsection