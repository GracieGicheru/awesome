@extends('layouts.app')

@section('content')
    <style>
        .tickets {
            border: 1px solid blue;

        }
    </style>
    <div class="container" style="margin-top: 150px;">
        <h1 class="text-center">Featured Events</h1>
        <div class="row justify-content-center">
            <div class="row col-md-12">
                @foreach($events as $event)
                <div class="col-md-3 tickets p-1 m-2" >
                    <img class="card-img-top" src="{{ url('storage/posters/'.$event->image) }}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"> {{$event->event_name}}</h5>
                        <p class="m-0">{{$event->location}}</p>
                        <p class="card-text m-0">{{\Carbon\Carbon::parse($event->start_date)->toDayDateTimeString()}}</p>
                        <a href="{{url('view-single-event/'.encrypt($event->id))}}" class="btn btn-primary float-right">Buy Now</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection