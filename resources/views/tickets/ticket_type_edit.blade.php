<div class="modal fade" id="editTicketType" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">      
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body" id="modal">
         <div class="container-fluid">
            <div class="card shadow mb-4">
               <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold"> Edit Ticket Type</h6>
               </div>
               <div class="card-body">              
                  
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                  
                     <div class="row">
                        <div class="col-lg-4 form-group">
                           <label><b>Name</b></label>
                           <input class="form-control required" id="ticket_name1" name="ticket_name1" type="text">
                        </div>
                        <div class="col-lg-4 form-group">
                        <label><b>Unit Cost</b></label>
                           <input class="form-control required" id="unit_price1" name="unit_price1" type="text">
                        </div>
                        <div class="col-lg-4 form-group">
                        <label><b>Ticket Available</b></label>
                           <input class="form-control required" id="tickets_available1" name="tickets_available1" type="text">
                        </div>
                        
                     </div>
                     <div class="row">
                     <div class="col-lg-4 form-group">
                  <label><b>Group Ticket</b></label>
                  <select class="form-control required" id="group_ticket1" name="group_ticket1">
                     <option value="No">No</option>
                     <option value="Yes">Yes</option>
                  </select>
			   </div>
			   <div class="col-lg-4 form-group">
                        <label><b>Ticket Sales Start</b></label>
                           <input class="form-control required" id="tickets_sales_start1" name="tickets_sales_start1" type="text">
				 </div>
				 <div class="col-lg-4 form-group">
                        <label><b>Tickets Sales End </b></label>
                           <input class="form-control required" id="tickets_sales_end1" name="tickets_sales_end1" type="text">
                   </div>
                     </div>
                   
                
                   
                     <input type="hidden" id="id" name="id"  class="form-control">
                     <button class="btn btn-submit btn-success btn-lg ml-2 editTicketButton" onclick="edit_ticketType()"  style="float: right;" type="submit">Update</button>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<script>
$(document).ready(function(){
$('input[name="tickets_sales_start1"]').datetimepicker({
timepicker: true,
minDate:'{{\Carbon\Carbon::today()}}',
});
$('input[name="tickets_sales_end1"]').datetimepicker({
timepicker: true,
minDate:'{{\Carbon\Carbon::today()}}',
});
});

function editTicketType(name,unit_price,tickets_available, group_ticket, ticket_sales_start, ticket_sales_end, id){
    $('#ticket_name1').val(name);
    $('#unit_price1').val(unit_price);
    $('#tickets_available1').val(tickets_available);
   //  $('#group_ticket1').val(group_ticket);
    $('#group_ticket1 option[value="' + group_ticket +'"]').prop("selected", true);
    $('#tickets_sales_start1').val(ticket_sales_start);
    $('#tickets_sales_end1').val(ticket_sales_end);
    $('#id').val(id);
  

}

function edit_ticketType() {
      id= $("#id").val();
		 $.ajax({
			  url: '/edit/ticket/type',
			  method: 'POST',
			  data:{
              id,
					'_token': $('#token').val(),
					'ticket_name1': $('#ticket_name1').val(),
               'unit_price1': $('#unit_price1').val(),
               'tickets_available1': $('#tickets_available1').val(),
					'group_ticket1': $('#group_ticket1').val(),
               'tickets_sales_start1': $('#tickets_sales_start1').val(),
               'tickets_sales_end1': $('#tickets_sales_end1').val(),
					
			  },

			  success:function(data){
              if(data.errors){
               toastr.error(data.errors)

              }else{
                toastr.success("Ticket Type edited sucessfully");           
					$("#ticket_type_div").replaceWith(data)
					$('#editTicketType').modal('hide')
              }
			  }
		 });
	}


</script>
