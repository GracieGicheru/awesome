<div id ="concession_div">
									<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

										<thead>
											<tr>
												<th>Name</th>
												<th>Quantity</th>
												<th>Unit Cost</th>
												<th>Units Sold</th>
												<th>Created By</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											@foreach($concessions as $concession)
											<tr class="concession_row"id="">
												<td>{{$concession->concession_name}}</td>
												<td>{{$concession->quantity}}</td>
												<td>{{$concession->unit_cost}}</td>
												<td></td>
												<td></td>
												<td> 
													<button class="btn  remove-concession" data-id="{{$concession->id}}" data-action="{{ route('concession.destroy',$concession->id) }}" data-toggle="tooltip" data-placement="top" title="Delete" style="color: red;">
													<i class="fas fa-trash"></></i> 
													</button>
													<button class="btn" data-placement="top" title="Edit" data-toggle="modal" data-target="#editConcession" onclick="return editConcession('{{ $concession->concession_name}}','{{$concession->quantity}}', '{{$concession->unit_cost}}', '{{$concession->individual_ticket}}', '{{$concession->id}}')" >
													<i class="fas fa-edit" style="color: green;" ></i>
													</button>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
</div>

<script type="text/javascript">
  $("body").on("click",".remove-concession",function(){
    var current_object = $(this);
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Concession!",
        type: "error",
        showCancelButton: true,
        dangerMode: true,
        cancelButtonClass: '#DD6B55',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Delete!',
    },function (result) {
        if (result) {
            var action = current_object.attr('data-action');
            var token = jQuery('meta[name="csrf-token"]').attr('content');
            var id = current_object.attr('data-id');

            $('body').html("<form class='form-inline remove-form' method='post' action='"+action+"'></form>");
            $('body').find('.remove-form').append('<input name="_method" type="hidden" value="delete">');
            $('body').find('.remove-form').append('<input name="_token" type="hidden" value="'+token+'">');
            $('body').find('.remove-form').append('<input name="id" type="hidden" value="'+id+'">');
            $('body').find('.remove-form').submit();
			// $("#promotion_div").replaceWith(data)
			// $('.remove-promotion').modal('toggle')
        }
    });
});
</script>