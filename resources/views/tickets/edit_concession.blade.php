<div class="modal fade" id="editConcession" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">      
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body" id="modal">
         <div class="container-fluid">
            <div class="card shadow mb-4">
               <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold"> Edit Concession</h6>
               </div>
               <div class="card-body">
                  @if ($errors->any())
                  <div class="alert alert-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
                  @endif
                  
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                     <div class="row">
                        <div class="col-lg-4 form-group">
                           <label><b>Name</b></label>
                           <input class="form-control required" id="concession_name1" name="concession_name1" type="text">
                        </div>
                        <div class="col-lg-4 form-group">
                        <label><b>Unit Cost</b></label>
                           <input class="form-control required" id="unit_cost1" name="unit_cost1" type="text">
                        </div>
                        <div class="col-lg-4 form-group">
                        <label><b>Quantity</b></label>
                           <input class="form-control required" id="quantity1" name="quantity1" type="text">
                        </div>
                        
                     </div>
                     <div class="row">
                     <div class="col-lg-4 form-group">
                  <label><b>Individual Ticket</b></label>
                  <select class="form-control required" id="individual_ticket1" name="individual_ticket1">
                     <option value="No">No</option>
                     <option value="Yes">Yes</option>
                  </select>
               </div>
                     </div>
                   
                
                   
                     <input type="hidden" id="id" name="id"  class="form-control">
                     <button class="btn btn-submit btn-success btn-lg ml-2 editConcessionButton" onclick="edit_Concessions()" style="float: right;" type="submit">Update</button>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<script>

function editConcession(concession_name,unit_cost,quantity, individual_ticket,id){
    $('#concession_name1').val(concession_name);
    $('#unit_cost1').val(unit_cost);
    $('#quantity1').val(quantity);
    $('#individual_ticket1').val(individual_ticket);
    $('#id').val(id);
   //  console.log("function id", id);

}

function edit_Concessions() {
      id= $("#id").val();
      console.log(id);
		 $.ajax({
			  url: '/editConcession',
			  method: 'POST',
			  data:{
              id,
					'_token': $('#token').val(),
					'concession_name1': $('#concession_name1').val(),
					'unit_cost1': $('#unit_cost1').val(),
					'quantity1': $('#quantity1').val(),
					'individual_ticket1': $('#individual_ticket1').val(),
					
			  },
			  success:function(data){

            if(data.errors){
               toastr.error(data.errors)

              }else{
                toastr.success("Ticket Type added sucessfully");           
					$("#concession_div").replaceWith(data)
					$('#editConcession').modal('hide')
              }
				
			  }
		 });
	}
</script>