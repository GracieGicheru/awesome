
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="ticket_type_modal">
				<div class="container-fluid">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold"> Add Ticket Type</h6>
						</div>
						<div class="card-body">
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

							<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
							<div class="row">
								<div class="col-lg-4 form-group">
									<label><b>Name</b></label>
									<input class="form-control required" id="ticket_type_name" name="ticket_type_name" type="text">
									<input type="hidden" name="event_id" id="event_id" value="{{$event->id}}">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Unit Price</b></label>
									<input class="form-control required" id="unit_price" name="unit_price" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Tickets Available</b></label>
									<input class="form-control required" id="tickets_available" name="tickets_available" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 form-group">
									<label><b>Group Ticket</b></label>
									<select class="form-control required" id="group_ticket" name="group_ticket">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</select>
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Ticket Sales Start</b></label>
									<input class="form-control required" id="ticket_sales_start" name="ticket_sales_start" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Ticket Sales End</b></label>
									<input class="form-control required" id="ticket_sales_end" name="ticket_sales_end" type="text">
								</div>
							</div>
							<button class="btn btn-success btn-lg ml-2 submitTicketButton" id="submitTicketButton" style="float: right;" onclick="addTicketType()">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	 function addTicketType() {
		 $.ajax({
			  url: '/addTicketType',
			  method: 'POST',
			  data:{
					'_token': $('#token').val(),
					'name': $('#ticket_type_name').val(),
					'unit_price': $('#unit_price').val(),
					'group_ticket': $('#group_ticket').val(),
					'tickets_available': $('#tickets_available').val(),
					'ticket_sales_start': $('#ticket_sales_start').val(),
					'ticket_sales_end': $('#ticket_sales_end').val(),
					'event_id':$('#event_id').val(),
			  },
			  success:function(data){

				if(data.errors){
               toastr.error(data.errors)

              }else{
                toastr.success("Ticket Type added sucessfully");           
					$("#ticket_type_div").replaceWith(data)
					$('#mediumModal').modal('hide')
              }
					
			  }
		 });
	}
</script>