<div class="modal fade" id="concession_add" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal">
				<div class="container-fluid">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold"> Add Concession</h6>
						</div>
						<div class="card-body">
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                            @endif
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
							<input type="hidden" value="{{$event->id}}" id="event_id" name="event_id"  class="form-control">
	
							<div class="row">
								<div class="col-lg-4 form-group">
									<label><b>Name</b></label>
									<input class="form-control required" id="concession_name" name="concession_name" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Unit Cost</b></label>
									<input class="form-control required" id="unit_cost" name="unit_cost" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Quantity</b></label>
									<input class="form-control required" id="quantity" name="quantity" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Generate Individual Ticket</b></label>
									<select class="form-control required" id="individual_ticket" name="individual_ticket">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</select>
								</div>
							</div>
						  <button class="btn btn-submit btn-success btn-lg ml-2 submitButton" id="submitConcessionButton" style="float: right;" onclick="addConcession()">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script> 
	 function addConcession() {
         console.log('this ');
		 $.ajax({
			  url: '/addConcession',
			  method: 'POST',
			  data:{
					'_token': $('#token').val(),
					'concession_name': $('#concession_name').val(),
					'unit_cost': $('#unit_cost').val(),
					'quantity': $('#quantity').val(),
					'individual_ticket': $('#individual_ticket').val(),
					'event_id':$('#event_id').val(),
					
			  },
			  success:function(data){
				if(data.errors){
               toastr.error(data.errors)

                }else{
                toastr.success("Ticket Type added sucessfully");           
					$("#concession_div").replaceWith(data)
					$('#concession_add').modal('hide')
              }
					
			  }
		 });
	}
</script>