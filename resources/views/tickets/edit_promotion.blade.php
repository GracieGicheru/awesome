<div class="modal fade" id="edit_promotion" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal">
				<div class="container-fluid">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold"> Edit Promotion</h6>
						</div>
						<div class="card-body">
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                            @endif
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
	
							<div class="row">
								<div class="col-lg-4 form-group">
									<label><b>Name</b></label>
									<input class="form-control required" id="promotion_name1" name="promotion_name1" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Number of coupons</b></label>
									<input class="form-control required" id="coupons1" name="coupons1" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Start Date</b></label>>
                                    <input class="form-control required" id="start_date1" name="start_date1" type="text" autocomplete="off">
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-4 form-group">
									<label><b>End Date</b></label>
                                    <input class="form-control required" id="end_date1" name="end_date1" type="text" autocomplete="off">
                                </div>
								<div class="col-lg-4 form-group">
									<label><b>Type</b></label>
									<select class="form-control required" id="type1" name="type1">
										<option value="Discount">Discount</option>
										<option value="Percentage">Percentage</option>
									</select>
                                </div>
                                <div class="col-lg-4 form-group">
									<label id= "change_type1"><b>Discount</b></label>
									<input class="form-control required" id="amount1" name="amount1" type="number">
								</div>             
                                </div>

							<input type="hidden" id="id" name="id"  class="form-control">
						  <button class="btn btn-submit btn-success btn-lg ml-2 editpromotionButton" id="editpromotionButton" style="float: right;" onclick="edit_promotion()">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script> 
$(document).ready(function(){
$('input[name="start_date1"]').datetimepicker({
timepicker: true,
minDate:'{{\Carbon\Carbon::today()}}',
});
$('input[name="end_date1"]').datetimepicker({
timepicker: true,
minDate:'{{\Carbon\Carbon::today()}}',
});
});


$(function(){
 $("#type1").on('change', function(){
	 var e=document.getElementById('type1')
	 if (e.options[e.selectedIndex].value=='Discount'){
	$('#change_type1').text('Discount').wrapInner("<strong />");
	 }else{
		$('#change_type1').text('Percentage').wrapInner("<strong />");

	 }


 })
  
});

function editPromotion(promotion_name,coupons,start_date, end_date,promotion_type,amount,id){
    $('#promotion_name1').val(promotion_name);
    $('#coupons1').val(coupons);
    $('#start_date1').val(start_date);
    $('#end_date1').val(end_date);
    $('#type1').val(promotion_type);
    $('#amount1').val(amount);
    $('#id').val(id);
   //  console.log("function id", id);

}


	function edit_promotion() {
         console.log('this ');
         id= $("#id").val();
		 $.ajax({
			  url: '/edit/promotion',
			  method: 'POST',
			  data:{
                  id,
					'_token': $('#token').val(),
					'promotion_name1': $('#promotion_name1').val(),
					'coupons1': $('#coupons1').val(),
					'start_date1': $('#start_date1').val(),
					'end_date1': $('#end_date1').val(),
					'type1': $('#type1').val(),
					'amount1': $('#amount1').val()
					
			  },
			  success:function(data){

			 if(data.errors){
               toastr.error(data.errors)

              }else{
                toastr.success("Promotion edited sucessfully");           
					$("#promotion_div").replaceWith(data)
					$('#edit_promotion').modal('hide')
              }
			  }
		 });
	}
</script>