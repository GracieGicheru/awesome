 @extends('layouts.home')

@section('content')

<div class="container-fluid">
   <div class="card shadow mb-4">
      <div class="card-header py-3">
         <h6 class="m-0 font-weight-bold"> Add Events</h6>
      </div>
      <div class="card-body">      


      	<form id="eventsForm"> 
@csrf

            <div class="row">
               <div class="col-lg-4 form-group">
                  <label><b>Event Name</b></label>
                  <input class="form-control required" id="event_name" name="event_name" type="text" required>
               </div>
               <div class="col-lg-4 form-group">
                  <label><b>Location</b></label>
                  <input class="form-control required" id="location" name="location" type="text">
               </div>
               <div class="col-lg-4 form-group">
                  <label><b>Organizer</b></label>
                  <select class="form-control required" id="organizer" name="organizer">
                      <option value="standard">Standard Group</option>

                  </select>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-4 form-group">
                  <label><b>Status</b></label>
                  <select class="form-control required" id="status" name="status">
                     <option value="Published">Published</option>
                     <option value="Pending">Pending</option>
                  </select>
               </div>
                <div class="col-lg-4 form-group">
                  <label><b>Event Frequency</b></label>
                  <select class="form-control required" id="frequency" name="frequency">
                     <option value="One Time">One Time</option>
                     <option value="Multiple Times">Multiple Times</option>
                  </select>
               </div>
                 <div class="col-lg-4 form-group">
                  <label><b>Start Date</b></label>
                  <input class="form-control required" id="start_date" name="start_date" type="text" autocomplete="off">
               </div>
              
            </div>
            <div class="row">
            <div class="col-lg-4 form-group">
                        <label><b>End Date</b></label>
                           <input class="form-control required" id="end_date" name="end_date" type="text" autocomplete="off">
				 </div>

               <div class="col-lg-4 form-group">
                  <label><b>Featured</b></label>
                  <select class="form-control required" id="featured" name="featured">
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>
                  </select>
               </div>
               <div class="col-lg-4 form-group">
                  <label><b>Currency</b></label>
                  <select class="form-control required" id="currency" name="currency">
                     <option value="Ksh">Ksh</option>
                     <option value="Dollar">USD</option>
                  </select>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-4 form-group">
                  <label><b>Enable Planned Seating</b></label>
                  <select class="form-control required" id="seating" name="seating">
                     <option value="No">No</option>
                     <option value="Yes">Yes</option>
                  </select>
               </div>
                                            <div class="form-group">
            <label class="image"><b>Poster</b></label>
            <input type="file" name="poster" id="poster" class="form-control">
          </div>

        
                     <div class="col-lg-4 form-group">
                        <label for="description" class=" form-control-label"><b>Description</b></label>
                          <textarea  class="form-control"
                          name="description" id="description"></textarea>
                                                </div>



            </div>
            <button class="btn btn-submit btn-success btn-lg ml-2" id="submitButton" type="submit">save</button>
        </form>
      


      </div>
      
      
   </div>
</div>

<script>

$(document).ready(function(){
$('input[name="start_date"]').datetimepicker({
timepicker: true,

minDate:'{{\Carbon\Carbon::today()}}',//yesterday is minimum date(for today use 0 or -1970/01/01)
});
$('input[name="end_date"]').datetimepicker({
timepicker: true,

minDate:'{{\Carbon\Carbon::today()}}',//yesterday is minimum date(for today use 0 or -1970/01/01)
});
});

$('#eventsForm').on('submit', function(event) {
    event.preventDefault();
  
    $.ajax({
      url:  "#",
      method: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      success: function(data) {
        console.log(data);
        if ((data.errors)) {
        //   $('#upload').removeAttr('hidden');
          toastr.error(data.errors);         
        } else {
        //   document.getElementById("select_file").value = "";
        //   $('#sendername').val(''); 
        //   $('#replacement').replaceWith(data);
        //   toastr.success("Sender name request received successfully");
        toastr.success(data);
        }
      },
        error: function(error) {
          toastr.error('Error occured adding the event');
        }
    })
  });
   
</script>
       

@endsection