 @extends('layouts.home')
 @section('content')


 <div class="container-fluid">
	<div class="card shadow mb-4">
		<div class="card-header">
			<h4 class="m-0 font-weight-bold text-muted">{{$event->event_name}}</h4>
			<h6 class="m-0 font-weight-bold text-muted">Event Details</h6>
		</div>
		<div class="card-body request">
			<div class="accordion child_view" id="accordionExample">
				<ul class="nav nav-tabs" id="schedule_report_tab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="create-role-tab"
							data-toggle="tab" href="#create_role" role="tab" aria-controls="create-role-tab" aria-selected="true">Summary</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="all_ticket_types"
							data-toggle="tab" href="#ticket_type" role="tab" aria-controls="all_ticket_types" aria-selected="false">Ticket Types
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tickets"
							data-toggle="tab" href="#tickets_role" role="tab" aria-controls="tickets" aria-selected="false">Tickets
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="all-orders"
							data-toggle="tab" href="#orders" role="tab" aria-controls="all-orders" aria-selected="false">Orders
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="all-promotions"
							data-toggle="tab" href="#promotions" role="tab" aria-controls="all-promotions" aria-selected="false">Promotions
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="all_concessions"
							data-toggle="tab" href="#concession" role="tab" aria-controls="all_concessions" aria-selected="false">Concessions
						</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<br />
					<div class="tab-pane fade show active" id="create_role" role="tabpanel" aria-labelledby="create-role-tab">
						<div class="card mb-4">
							<div class="card-body">
							@include('tickets.page')
								<div class="card-body">
									<input type="hidden" id="event_id" value="{{ $event->id }}">
									<img src="{{ url('storage/posters/'.$event->image) }}" height="auto" width="25%" > 
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade ticket_type " id="ticket_type" role="tabpanel" aria-labelledby="all_ticket_types">
						<div class="card mb-4">
							<div class="card-body">
								@include('tickets.page')

								<div class="table-responsive">
									<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
									<button class="btn btn-submit btn-success" ata-placement="top" title="Add" data-toggle="modal" data-target="#mediumModal" style="margin-bottom: 20px;"  type="submit">Add Ticket Type</button>
									 <div id="ticket_type_div">
										@include('tickets.ticket_type_table')
									</div>
									@include('tickets.ticket_type_add')

									<div id="edit_ticket_type_div">
										@include('tickets.ticket_type_edit')
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade tickets_role " id="tickets_role" role="tabpanel" aria-labelledby="tickets">
						<div class="card mb-4">
							<div class="card-body">
							@include('tickets.page')
								
								<div id="tickets_div"></div>
							</div>
						</div>
					</div>
				
					<div class="tab-pane fade orders " id="orders" role="tabpanel" aria-labelledby="all-orders">
						<div class="card mb-4">
							<div class="card-body">
							@include('tickets.page')
								
								<div id="orders_div">
								@include('tickets.orders_datatable')
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade promotions " id="promotions" role="tabpanel" aria-labelledby="all-promotions">
						<div class="card mb-4">
							<div class="card-body">
							   @include('tickets.page')
							   <div class="table-responsive">
									<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
									<button class="btn btn-submit btn-success" ata-placement="top" title="Add" data-toggle="modal" data-target="#add_promotion" style="margin-bottom: 20px;"  type="submit">Add Promotion</button>
									 <div id="promotions_div">
										@include('tickets.promotion_datatable')
										@include('tickets.promotion_add')
										@include('tickets.edit_promotion')
									</div>
								
							</div>
						</div>
					</div>
					</div>
					<div class="tab-pane fade concession " id="concession" role="tabpanel" aria-labelledby="all_concessions">
						<div class="card mb-4">
							<div class="card-body">
								@include('tickets.page')
						
								<div class="table-responsive">
									<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
									<button class="btn btn-submit btn-success" ata-placement="top" title="Add" data-toggle="modal" data-target="#concession_add" style="margin-bottom: 20px;"  type="submit">Add Concession</button>
									 <div id="ticket_type_div">
										@include('tickets.concession_datatable')
										@include('tickets.concession_add')
										@include('tickets.edit_concession')
									</div>




							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>






 <script>
 $(document).ready(function(){
$('input[name="ticket_sales_start"]').datetimepicker({
timepicker: true,
minDate:'{{\Carbon\Carbon::today()}}',
});
$('input[name="ticket_sales_end"]').datetimepicker({
timepicker: true,
minDate:'{{\Carbon\Carbon::today()}}',
});
});

	
	$('#sendsingledate').datetimepicker();
	

 </script>




	
@endsection