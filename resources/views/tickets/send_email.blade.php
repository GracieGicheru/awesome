<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Standard Tickets</title>
    <style>
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none; background:#E2E2E2;}
        body {margin:0; padding:0; font-family:'arial', sans-serif; color:#58595b; font-size:14px;}
        table td {border-collapse:collapse;}
        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
        a, img {border:none; }
        a:link, a:hover, a:active, a:visited{color:#FFFFFF !important; text-decoration:none !important; cursor:pointer;}
        a:hover{ text-decoration:none !important;}
        a {-webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;}
        p{ font-size:13px;}
        table td { border-collapse:collapse; }

    </style>
</head>
<body>
<table bgcolor="#FFFFFF" width="460" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;" align="center">
    <tr>
        <td align="left" valign="top">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#232323">
                <tr>
                    <td width="100" align="left" valign="top"></td>
                    <td width="260" align="center" valign="top">
                        <p style="margin-top: 30px;">
                            
                        </p>
                        <h2 style="font-family:'arial',sans-serif;color: #fff;text-align: center; font-size: 26px;font-weight: 700; margin-bottom: 0;"></h2>
                        <p style="font-family:'arial',sans-serif;color: #767676;font-size: 14px; margin-top: 5px; margin-bottom: 25px;">Event</p>
                    </td>
                    <td width="100" align="left" valign="top"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="border-bottom: 1px dashed #ccc">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
                <tr>
                    <td width="40" align="left" valign="top"></td>
                    <td width="380" align="center" valign="top">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
                            <tr>
                                <td width="230" align="left" valign="top">
                                    <p style="font-family:'arial',sans-serif;color: #898989; margin: 30px 0 0;font-size: 13px;"><b>Date</b></p>
                                    <h3 style="font-family:'arial',sans-serif;color: #1F1D1D; margin-top: 5px;margin-bottom: 40px;font-size: 14px;"></h3>
                                </td>
                                <td width="150" align="left" valign="top">
                                    <p style="font-family:'arial',sans-serif;color: #898989;margin: 30px 0 0;font-size: 13px;"><b>Price</b></p>
                                    <h3 style="font-family:'arial',sans-serif;color: #1F1D1D; margin-top: 5px; margin-bottom: 40px;font-size: 14px;">
                                        <span style="display: block;margin-bottom: 5px;">Ksh</span>
                                    </h3>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="border-bottom: 1px dashed #ccc">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
                <tr>
                    <td width="40" align="left" valign="top"></td>
                    <td width="380" align="center" valign="top">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
                            <tr>
                                <td width="230" align="left" valign="top">
                                    <p style="font-family:'arial',sans-serif;color: #898989; margin: 30px 0 0;font-size: 13px;"> <b>Tickets type</b></p>
                                    <h3 style="font-family:'arial',sans-serif;color: #1F1D1D; margin-top: 5px;margin-bottom: 40px;font-size: 14px;"></h3>
                                </td>
                               
                                    <td width="150" align="left" valign="top">
                                        <p style="font-family:'arial',sans-serif;color: #898989;margin: 30px 0 0;font-size: 13px;"> <b>Concessions</b></p>
                                      
                                                <h3 style="font-family:'arial',sans-serif;color: #1F1D1D; margin-top: 5px; margin-bottom: 40px;font-size: 14px;">
                                                    <span style="display: block;margin-bottom: 5px;"></span>
                                                </h3>
                                         
                                    </td>
                              
                            </tr>
                        </table>
                    </td>
                    <td width="40" align="left" valign="top"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="border-bottom: 1px dashed #ccc">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
                <tr>
                    <td width="40" align="left" valign="top"></td>
                    <td width="380" align="center" valign="top">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
                            <tr>
                                <td width="230" align="left" valign="top">
                                    <p style="font-family:'arial',sans-serif;color: #898989; margin: 30px 0 0;font-size: 13px;"><b>Booked by</b></p>
                                    <h3 style="font-family:'arial',sans-serif;color: #1F1D1D; margin-top: 5px;margin-bottom: 40px;font-size: 14px;">
                                        </h3>
                                    <p style="font-family:'arial',sans-serif;color: #898989;margin: 30px 0 0;font-size: 13px;"> <b>Email</b></p>
                                    <h3 style="font-family:'arial',sans-serif;color: #1F1D1D; margin-top: 5px; margin-bottom: 40px;font-size: 14px;"></h3>
                                </td>
                                <td width="150" align="left" valign="top">
                                    <p style="font-family:'arial',sans-serif;color: #898989;margin: 30px 0 0;font-size: 13px;"><b>Phone number</b></p>
                                    <h3 style="font-family:'arial',sans-serif;color: #1F1D1D; margin-top: 5px; margin-bottom: 40px;font-size: 14px;"></h3>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="40" align="left" valign="top"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="border-bottom: 1px solid #e2e2e2;">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
                <tr>
                    <td width="40" align="left" valign="top"></td>
                    <td width="380" align="center" valign="top">
                        <p style="width: 126px;height: 120px; margin: 20px auto;">
                        <img src="{{ url('qr.png') }}" width="126" height="120" />
                           
                        </p>
                    </td>
                    <td width="40" align="left" valign="top"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#F1F1F1">
                <tr>
                    <td width="40" align="left" valign="top"></td>
                    <td width="380" align="center" valign="top">
                        <p style="color:#898989; font-size: 12px; margin: 15px 0; text-align: center;"><b>Ticket no. </b></p>
                    </td>
                    <td width="40" align="left" valign="top"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>