<div id ="promotion_div">
									<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<meta name="csrf-token" content="{{ csrf_token() }}">

										<thead>
											<tr>
												<th>Name</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Type</th>
												<th>Amount</th>
												<th>Created By</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											@foreach($promotions as $promotion)
											<tr class=""id="">
												<td>{{$promotion->promotion_name}}</td>
												<td>{{$promotion->start_date}}</td>
												<td>{{$promotion->end_date}}</td>
												<td>{{$promotion->promotion_type}}</td>
												<td>{{$promotion->amount}}</td>
												<td></td>
												<td> 
												<button class="btn" data-placement="top" title="Edit" data-toggle="modal" id="{{$promotion->id}}"  data-target="#edit_promotion" onclick="return editPromotion('{{ $promotion->promotion_name}}','{{$promotion->coupons}}','{{$promotion->start_date}}', '{{$promotion->end_date}}', '{{$promotion->promotion_type}}','{{$promotion->amount}}', '{{$promotion->id}}')" >
													<i class="fas fa-edit" style="color: green;" ></i>
													</button>
													<button class="btn" data-placement="top" title="View"  id="{{$promotion->id}}" onclick="viewPromo('{{$promotion->id}}')"  >
													<i class="fas fa-info-circle"></></i>
													</button>

													<button class="btn  remove-promotion" data-id="{{ $promotion->id }}"  data-action="{{ route('promotions.destroy',$promotion->id) }}" data-toggle="tooltip" data-placement="top" title="Delete" style="color: red;">
													<i class="fas fa-trash"></></i> 
													</button>
													
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>


								
</div>

<div class="modal fade" id="view_promo" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal">
				<div class="container-fluid">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold"> Available Coupons</h6>
						</div>
						<div class="card-body">

                        <span id="load-coupons">


</span>
                               

						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
  $("body").on("click",".remove-promotion",function(){
    var current_object = $(this);
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this promotion!",
        type: "error",
        showCancelButton: true,
        dangerMode: true,
        cancelButtonClass: '#DD6B55',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Delete!',
    },function (result) {
        if (result) {
            var action = current_object.attr('data-action');
            var token = jQuery('meta[name="csrf-token"]').attr('content');
            var id = current_object.attr('data-id');

            $('body').html("<form class='form-inline remove-form' method='post' action='"+action+"'></form>");
            $('body').find('.remove-form').append('<input name="_method" type="hidden" value="delete">');
            $('body').find('.remove-form').append('<input name="_token" type="hidden" value="'+token+'">');
            $('body').find('.remove-form').append('<input name="id" type="hidden" value="'+id+'">');
            $('body').find('.remove-form').submit();
			$("#promotion_div").replaceWith(data)
			// $('.remove-promotion').modal('toggle')
        }
    });
});

function viewPromo(promotion_id){

	var url = "{{ url('coupon') }}";
	$.get(url+'/'+promotion_id,function(res){
	 $('#load-coupons').html(res)
	 $('#view_promo').modal ('show')
	 

	});
}
</script>
