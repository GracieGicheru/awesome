<div id="ticket_type">
<table class="table table-bordered" id="ticket_types_datatable" width="100%" cellspacing="0">
   <thead>
      <tr>
         <th>Name</th>
         <th>Price</th>
         <th>Group Ticket</th>
         <th>Sales</th>
         <th>Tickets Sold</th>
         <th>Tickets Available</th>
         <th>Actions</th>
      </tr>
   </thead>
   <tbody>
      @foreach($ticketTypes as $ticket)
         <tr id="" style="height: 2px;">
            <td>{{ $ticket->name}}</td>
            <td>{{$ticket->unit_price}}</td>
            <td>{{$ticket->group_ticket}}</td>
            <td>{{$ticket->sales}}</td>
            <td>{{$ticket->sold}}</td>
            <td>{{$ticket->tickets_available}}</td>
            <td> 
               
				<button class="btn  remove_ticket_type" data-id="{{ $ticket->id }}"  data-action="{{ route('ticket.destroy',$ticket->id) }}" data-toggle="tooltip" data-placement="top" title="Delete">
					<i class="fa fa-trash text-danger"></></i> 
				</button>
               <button class="btn" data-placement="top" title="Edit" data-toggle="modal" id="" data-target="#editTicketType"  onclick="return editTicketType('{{ $ticket->name}}','{{$ticket->unit_price}}','{{$ticket->tickets_available}}', '{{$ticket->group_ticket}}', '{{$ticket->ticket_sales_start}}', '{{$ticket->ticket_sales_end}}','{{$ticket->id}}')" >
               <i class="fa fa-edit text-success"></i>
               </button>
            </td>
         </tr>
      @endforeach
   </tbody>
</table>
</div>
<script>
   $(document).ready(function() {
   $('#ticket_types_datatable').DataTable() 
   });

   $("body").on("click",".remove_ticket_type",function(){
    var current_object = $(this);
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Ticket Type!",
        type: "error",
        showCancelButton: true,
        dangerMode: true,
        cancelButtonClass: '#DD6B55',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Delete!',
    },function (result) {
        if (result) {
            var action = current_object.attr('data-action');
            var token = jQuery('meta[name="csrf-token"]').attr('content');
            var id = current_object.attr('data-id');

            $('body').html("<form class='form-inline remove-form' method='post' action='"+action+"'></form>");
            $('body').find('.remove-form').append('<input name="_method" type="hidden" value="delete">');
            $('body').find('.remove-form').append('<input name="_token" type="hidden" value="'+token+'">');
            $('body').find('.remove-form').append('<input name="id" type="hidden" value="'+id+'">');
            $('body').find('.remove-form').submit();
			// $("#ticket_type").replaceWith(data)
			// $('.remove-promotion').modal('toggle')
        }
    });
});
</script>


