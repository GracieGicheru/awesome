<div class="modal fade" id="pay-modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal">
				<div class="container-fluid">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold"> Mobile Number</h6>
						</div>
						<div class="card-body">
						<div class="row">
								<div class="col-lg-6 form-group">
									<label><b>Enter Phone Number</b></label>
									<input class="form-control required" id="telephone" name="telephone" type="text">
								</div>
								<button class="btn btn-success btn-lg ml-2 submitPayButton" id="submitPayButton" style="float: right;">Save</button>
								


                        </div>			
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
