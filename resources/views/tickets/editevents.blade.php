 @extends('layouts.home')

@section('content')
<div class="modal fade" id="mediumModal2" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body" id="modal">
         <div class="container-fluid">
            <div class="card shadow mb-4">
               <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold"> Add Events</h6>
               </div>
               <div class="card-body">
                  @if ($errors->any())
                  <div class="alert alert-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
                  @endif
                  <form action="addevents" method="POST" enctype="multipart/form-data" id="eventsForm ">
                     @csrf
                     <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
                     <div class="row">
                        <div class="col-lg-4 form-group">
                           <label><b>Event Name</b></label>
                           <input class="form-control required" id="event_name1" name="event_name1" type="text">
                        </div>
                        <div class="col-lg-4 form-group">
                           <label><b>Location</b></label>
                           <input class="form-control required" id="location1" name="location1" type="text">
                        </div>
                        <div class="col-lg-4 form-group">
                           <label><b>Organizer</b></label>
                           <select class="form-control required" id="organizer1" name="organizer1">
                              <option value="standard">Standard Group</option>
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-4 form-group">
                           <label><b>Status</b></label>
                           <select class="form-control required" id="status1" name="status1">
                              <option value="Published">Published</option>
                              <option value="Pending">Pending</option>
                           </select>
                        </div>
                        <div class="col-lg-4 form-group">
                           <label><b>Event Frequency</b></label>
                           <select class="form-control required" id="frequency1" name="frequency1">
                              <option value="One Time">One Time</option>
                              <option value="Multiple Times">Multiple Times</option>
                           </select>
                        </div>
                        <div class="col-lg-4 form-group">
                           <label><b>Start Date</b></label>
                           <input class="form-control required" id="start_date1" name="start_date1" type="text" autocomplete="off">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-4 form-group">
                           <label><b>End Date</b></label>
                           <input class="form-control required" id="end_date1" name="end_date1" type="text" autocomplete="off">
                        </div>
                        <div class="col-lg-4 form-group">
                           <label><b>Featured</b></label>
                           <select class="form-control required" id="featured1" name="featured1">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                           </select>
                        </div>
                        <div class="col-lg-4 form-group">
                           <label><b>Currency</b></label>
                           <select class="form-control required" id="currency1" name="currency1">
                              <option value="Ksh">Kenya Shillings</option>
                              <option value="USD">USD</option>
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-4 form-group">
                           <label><b>Enable Planned Seating</b></label>
                           <select class="form-control required" id="seating1" name="seating1">
                              <option value="No">No</option>
                              <option value="Yes">Yes</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label class="image"><b>Poster</b></label>
                           <input type="file" name="poster" id="poster" class="form-control">
                        </div>
                        <div class="col-lg-4 form-group">
                           <label for="description" class=" form-control-label"><b>Description</b></label>
                           <textarea  class="form-control"
                              name="description1" id="description1"></textarea>
                        </div>
                     </div>
                     <button class="btn btn-submit btn-success btn-lg ml-2" id="editButton" type="submit">save</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<script>

$(document).ready(function(){
$('input[name="start_date1"]').datetimepicker({
timepicker: true,

minDate:'{{\Carbon\Carbon::today()}}',//yesterday is minimum date(for today use 0 or -1970/01/01)
});
$('input[name="end_date1"]').datetimepicker({
timepicker: true,

minDate:'{{\Carbon\Carbon::today()}}',//yesterday is minimum date(for today use 0 or -1970/01/01)
});
});
   
</script>


  
       
 <!-- <script src="js/events.js"></script> -->
@endsection