  @extends('layouts.home')
  @section('content')

   <div class="container-fluid">
  
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-danger">Payment Methods</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                  <button class="btn btn-submit btn-success" ata-placement="top" title="Edit" data-toggle="modal" data-target="#mediumModal" style="margin-bottom: 20px;"  type="submit">Add payment</button>
                  <thead>
                    <tr>
                      <th>Payment Methods </th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                
                  <tbody>
                     @foreach ($payments as $payment)

                    <tr id="tr{{$payment->id}}" >
                      <td> {{ $payment->payment }}</td>
                      <td> 
                        <button class="btn  deletePayment" data-toggle="tooltip" data-placement="top" title="Delete" id="del{{$payment->id}}" style="color: red;">

                                                    <i class="fas fa-trash"></></i> 
                                                              <button class="btn" data-placement="top" title="Edit" data-toggle="modal" id="{{$payment->id}}" data-target="#mediumModal2"  onclick="return editPayment('{{ $payment->payment}}','{{$payment->id}}')">
                                                    <i class="fas fa-edit" style="color: green;"></i>
                                                </button>                      </td>
                    </tr>

                    @endforeach 
            
               
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>




<div class="modal fade" id="mediumModal2" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">      
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body" id="modal">
         <div class="container-fluid">
            <div class="card shadow mb-4">
               <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold"> Edit Payment</h6>
               </div>
               <div class="card-body">
                  @if ($errors->any())
                  <div class="alert alert-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
                  @endif
                  <form action="editpayment" method="POST" id="paymentForm ">
                     @csrf
                     <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
                     <div class="row">
                        <div class="col-lg-6 form-group">
                           <label><b>Payment</b></label>
                           <input class="form-control required" id="payment1" name="payment1" type="text">
                        </div>
                        
                     </div>
                   
                
                   
                     <input type="hidden" id="id" name="id" class="form-control">
                     <button class="btn btn-submit btn-success btn-lg ml-2 editPaymentButton" style="float: right;" type="submit">Update</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>



<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">    
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body" id="modal">
         <div class="container-fluid">
            <div class="card shadow mb-4">
               <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold"> Add Payment Method</h6>
               </div>
               <div class="card-body">
                  @if ($errors->any())
                  <div class="alert alert-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
                  @endif
                  <form action="/addPayment" method="POST" >
                     <!-- @csrf -->
                     <meta name="csrf-token" content="{{ csrf_token() }}">
                     <div class="row">
                        <div class="col-lg-6 form-group">
                           <label><b>Payment methods</b></label>
                           <input class="form-control required" id="payment" name="payment" type="text">
                        </div>
                      </div>
                     
                     <button class="btn btn-submit btn-success btn-lg ml-2 paymentSubmitButton" id="paymentSubmitButton" style="float: right;">Save</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

 <script src="js/settings.js"></script>



        @endsection
