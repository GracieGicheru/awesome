<div id ="promotion_div">
									<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<meta name="csrf-token" content="{{ csrf_token() }}">

										<thead>
											<tr>
												<th>Name</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Type</th>
												<th>Amount</th>
												<th>Created By</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											@foreach($promotions as $promotion)
											<tr class=""id="">
												<td>{{$promotion->promotion_name}}</td>
												<td>{{$promotion->start_date}}</td>
												<td>{{$promotion->end_date}}</td>
												<td>{{$promotion->promotion_type}}</td>
												<td>{{$promotion->amount}}</td>
												<td></td>
												<td> 
												<button class="btn" data-placement="top" title="Edit" data-toggle="modal" id="{{$promotion->id}}"  data-target="#edit_promotion" onclick="return editPromotion('{{ $promotion->promotion_name}}','{{$promotion->coupons}}','{{$promotion->start_date}}', '{{$promotion->end_date}}', '{{$promotion->promotion_type}}','{{$promotion->amount}}', '{{$promotion->id}}')" >
													<i class="fas fa-edit" style="color: green;" ></i>
													</button>
													<button class="btn" data-placement="top" title="View"  id="{{$promotion->id}}" onclick="viewPromo('{{$promotion->id}}')"  >
													<i class="fas fa-info-circle"></></i>
													</button>

													<button class="btn  remove-promotion" data-id="{{ $promotion->id }}"  data-action="{{ route('promotions.destroy',$promotion->id) }}" data-toggle="tooltip" data-placement="top" title="Delete" style="color: red;">
													<i class="fas fa-trash"></></i> 
													</button>
													
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>


								
</div>
