@extends('layouts.app')

@section('content')

<div class="container-fluid">
   <div class="card shadow mb-4">
      <div class="card-header py-3">
         <h6 class="m-0 font-weight-bold">Tickets </h6>
      </div>
      <div class="card-body">
      			@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

      	<form action= "{{url('add-concession')}}" method="POST" id="frontEndForm"> 

          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          <input type="hidden" name="order_id" id="order_id" value="{{ $order->id }}">
          <input type="hidden" name="event_id" id="event_id" value="{{ $events->id }}">
          <!-- @csrf -->

          <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
          <div class="concession_sale">
         @foreach($tickets as $ticket)

         <div class="row">
         <legend style="margin-left:20px;"><b>{{ $ticket->tickets}}</b></legend>
         </div>

            <div class="row">
              
               <div class="col-lg-4 form-group">
                  <label><b>Name</b></label>
                  <input class="form-control required" id="ticket[{{$ticket->id}}][name]" name="ticket[{{$ticket->id}}][name]" type="text">
               </div>
               <div class="col-lg-4 form-group">
                  <label><b>Email</b></label>
                  <input class="form-control required" id="ticket[{{$ticket->id}}][email]" name="ticket[{{$ticket->id}}][email]" type="email">
               </div>
               <div class="col-lg-4 form-group">
                  <label><b>Phone</b></label>
                  <input class="form-control required" id="ticket[{{$ticket->id}}][phone]" name="ticket[{{$ticket->id}}][phone]" type="text">
               </div>
            </div>

            <div class="row">
               <div class="col-lg-4 form-group">
               <label><b>Add Concession</b></label>
                  <select class="form-control required" id="ticket[{{$ticket->id}}][add_concession]" name="ticket[{{$ticket->id}}][add_concession]">
                     @foreach($concessions as $concession)
					 <option value="{{ $concession->id  }}"> {{ $concession->concession_name  }} </option>
					 @endforeach
				  </select>
               </div>
               <div class="col-lg-4 form-group">
               <label><b>Quantity</b></label>
               <input class="form-control required" id="ticket[{{$ticket->id}}][quantity]" name="ticket[{{$ticket->id}}][quantity]" type="number">
               </div>
               <!-- <div class="col-lg-4 form-group">
                  <button class="btn btn-submit btn-success btn-sm concessionButton" style="margin-top:30px;" type="submit">Add</button>
               </div> -->
            </div>
            @endforeach

          </div>


            <button class="btn btn-submit btn-success btn-lg ml-2" id="submitConcessionButton" type="submit">Submit</button>
        </form>
      


      </div>
      
      
   </div>
</div>

  @endsection    