  @extends('layouts.home')
  @section('content')

   <div class="container-fluid">
  
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-danger">Currencies</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                  <button class="btn btn-submit btn-success" ata-placement="top" title="Add" data-toggle="modal" data-target="#mediumModal" style="margin-bottom: 20px;"  type="submit">Add Currency</button>
                  <thead>
                    <tr>
                      <th>Currency</th>
                      <th>Code</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                
                  <tbody>
                     @foreach ($currencies as $currency)

                    <tr id="tr{{$currency->id}}">
                      <td> {{ $currency->name }}</td>
                      <td>{{ $currency->code }}</td>
                      <td> 
                     <button class="btn  deleteCurrency" id="del{{$currency->id}}" data-toggle="tooltip" data-placement="top" title="Delete" style="color: red;">

                                                    <i class="fas fa-trash"></></i> 
                                                </button>                                                  <button class="btn" data-placement="top" title="Edit" data-toggle="modal" id="{{$currency->id}}" data-target="#mediumModal2"  onclick="return editCurrency('{{ $currency->name}}','{{ $currency->code}}','{{$currency->id}}')">
                                                    <i class="fas fa-edit" style="color: green;"></i>
                                                </button>
                      </td>
                    </tr>

                    @endforeach 
            
               
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>




<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">    
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body" id="modal">
         <div class="container-fluid">
            <div class="card shadow mb-4">
               <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold"> Add Currency</h6>
               </div>
               <div class="card-body">
                  @if ($errors->any())
                  <div class="alert alert-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
                  @endif
                  <form action="/addCurrency" method="POST" >
                     <!-- @csrf -->
                     <meta name="csrf-token" content="{{ csrf_token() }}">
                     <div class="row">
                        <div class="col-lg-6 form-group">
                           <label><b>Name</b></label>
                           <input class="form-control required" id="name" name="name" type="text">
                        </div>
                        <div class="col-lg-6 form-group">
                           <label><b>Code</b></label>
                           <input class="form-control required" id="code" name="code" type="text">
                        </div>
                      
                     </div>
                     <button class="btn btn-submit btn-success btn-lg ml-2 submitButton" id="submitButton" style="float: right;">Save</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>



<div class="modal fade" id="mediumModal2" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">      
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body" id="modal">
         <div class="container-fluid">
            <div class="card shadow mb-4">
               <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold"> Edit Currency</h6>
               </div>
               <div class="card-body">
                  @if ($errors->any())
                  <div class="alert alert-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
                  @endif
                  <form action="editCurrency" method="POST" id="currencyForm ">
                     @csrf
                     <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
                     <div class="row">
                        <div class="col-lg-6 form-group">
                           <label><b>Name</b></label>
                           <input class="form-control required" id="name1" name="name1" type="text">
                        </div>
                        <div class="col-lg-6 form-group">
                           <label><b>Code</b></label>
                           <input class="form-control required" id="code1" name="code1" type="text">
                        </div>
                     </div>
                   
                
                   
                     <input type="hidden" id="id" name="id" class="form-control">
                     <button class="btn btn-submit btn-success btn-lg ml-2 editCurrencyButton" style="float: right;" type="submit">Update</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>


 <script src="js/settings.js"></script>


        @endsection
