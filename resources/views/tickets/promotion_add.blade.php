<div class="modal fade" id="add_promotion" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal">
				<div class="container-fluid">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold"> Add Promotion</h6>
						</div>
						<div class="card-body">
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							
							<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
							<input type="hidden" id="id" name="id"  class="form-control">
							<input type="hidden" value="{{$event->id}}" id="event_id" name="event_id"  class="form-control">
	
							<div class="row">
								<div class="col-lg-4 form-group">
									<label><b>Name</b></label>
									<input class="form-control required" id="promotion_name" name="promotion_name" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Number of coupons</b></label>
									<input class="form-control required" id="coupons" name="coupons" type="text">
								</div>
								<div class="col-lg-4 form-group">
									<label><b>Start Date</b></label>
                                    <input class="form-control required" id="start_date" name="start_date" type="text" autocomplete="off">
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-4 form-group">
									<label><b>End Date</b></label>
                                    <input class="form-control required" id="end_date" name="end_date" type="text" autocomplete="off">
                                </div>
								<div class="col-lg-4 form-group">
									<label><b>Type</b></label>
									<select class="form-control required" id="type" name="type">
										<option value="Discount">Discount</option>
										<option value="Percentage">Percentage</option>
									</select>
                                </div>
                                <div class="col-lg-4 form-group">
									<label id= "change_type"><b>Discount</b></label>
									<input class="form-control required" id="amount" name="amount" type="number">
								</div>
								

                                
                                </div>
							
						  <button class="btn btn-submit btn-success btn-lg ml-2 submitButton" id="submitConcessionButton" style="float: right;" onclick="addPromotion()">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script> 
$(document).ready(function(){
$('input[name="start_date"]').datetimepicker({
timepicker: true,
minDate:'{{\Carbon\Carbon::today()}}',
});
$('input[name="end_date"]').datetimepicker({
timepicker: true,
minDate:'{{\Carbon\Carbon::today()}}',
});
});


$(function(){
 $("#type").on('change', function(){
	 var e=document.getElementById('type')
	 if (e.options[e.selectedIndex].value=='Discount'){
	$('#change_type').text('Discount').wrapInner("<strong />");
	 }else{
		$('#change_type').text('Percentage').wrapInner("<strong />");

	 }


 })
  
});


	function addPromotion() {
         console.log('this ');
		 $.ajax({
			  url: '/add/promotion',
			  method: 'POST',
			  data:{
					'_token': $('#token').val(),
					'promotion_name': $('#promotion_name').val(),
					'coupons': $('#coupons').val(),
					'start_date': $('#start_date').val(),
					'end_date': $('#end_date').val(),
					'type': $('#type').val(),
					'amount': $('#amount').val(),
					'event_id': $('#event_id').val()

			  },
			  success:function(data){
				
			   if(data.errors){
                 toastr.error(data.errors)

              }else{
                toastr.success("Promotion added sucessfully");           
					$("#promotion_div").replaceWith(data)
					$('#add_promotion').modal('hide')
              }
			  }
		 });
	}
</script>