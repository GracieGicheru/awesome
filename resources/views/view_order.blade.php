@extends('layouts.app')

@section('content')
    <style>
    </style>
    <div class="container" style="margin-top: 50px;  border: 1px solid blue;">
        <h1 class="text-center"> Order Details</h1>

        <div id ="view_div">

        <div class="row">
                     <div class="col-lg-6 form-group">
                     <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<meta name="csrf-token" content="{{ csrf_token() }}">

										<thead>
											<tr>
												<th>Event Details</th>
												<th>Description</th>
			
											</tr>
										</thead>
										<tbody>
		
											<tr>
												<td>Event Name</td>
												<td>{{$events->event_name}}</td>
												
                                            </tr>
                                            
											<tr>
												<td>Start Time</td>
												<td>{{$events->start_date}}</td>
												
                                            </tr>
                                            
											<tr>
												<td>End Time</td>
												<td>{{$events->end_date}}</td>
												
                                            </tr>
                                            
											<tr>
												<td>Location</td>
												<td>{{$events->location}}</td>
												
                                            </tr>

										</tbody>
									</table>
			   </div>
			   <div class="col-lg-6 form-group">

               <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<meta name="csrf-token" content="{{ csrf_token() }}">

										<thead>
											<tr>
												<th>Ticket Details</th>
												<th>Description</th>
			
											</tr>
										</thead>
										<tbody>
                                            @foreach($tickets as $ticket)
		
											<tr>
												<td>Tickets</td>
												<td>{{$ticket->tickets}}</td>
												
                                            </tr>
                                            @endforeach

										</tbody>
                                    </table>
                                    
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<meta name="csrf-token" content="{{ csrf_token() }}">

										<thead>
											<tr>
												<th>Order Amount</th>
												<th>Description</th>
			
											</tr>
										</thead>
										<tbody>
		
											<tr>
												<td>Total Amount</td>
												<td>{{$order->amount}}</td>
												
											</tr>

										</tbody>
									</table>
                        
				 </div>
				 
                     </div>
									


								
</div>


<button class="btn btn-submit btn-success btn-lg ml-2 payButton" data-placement="top" title="Pay" data-toggle="modal" data-target="#pay-modal" style="float: right; margin-top:20px; margin-right:80px;" type="submit">Pay Now</button>


<div class="modal fade" id="pay-modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal">
				<div class="container-fluid">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold"> Mobile Number</h6>
						</div>
						<div class="card-body">
							<form action="{{url('pay-order')}}" method="POST" id="payment">
							<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
							<input type="hidden" id="order_id" name="order_id"  value="{{$order->id}}">

						<div class="row">
								<div class="col-lg-6 form-group">
									<label><b>Enter Phone Number</b></label>
									<input class="form-control required" id="phone" name="phone" type="text">
								</div>

						</div>
						
						
					<button class="btn btn-success btn-lg ml-2 submitPayButton" id="submitPayButton" style="float: right;">Save</button>
                  </form>
				</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection