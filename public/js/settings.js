$('.submitButton').click(function(e){
	e.preventDefault();
	console.log("this")

$.ajax({

	type: 'POST',
	url: '/addCurrency',
	  headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data:{
    	'name': $('input[id= name]').val(),
    	'code': $('input[id= code]').val(),
    },
    dataType:"json",

    success:function(data){
    	if (data.status=='ok'){

    		window.location.reload(true);
    	}
    }


});

});

function editCurrency(name,code,id){
    $('#name1').val(name);
    $('#code1').val(code);
    $('#id').val(id);

}
$('.editCurrencyButton').click(function(e){
    e.preventDefault();
    console.log('This');
    let id=$(this).prop('id');
    console.log(id);

    $.ajax({
        type:'POST',
        url:'/editCurrency',
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
        data:{
            'id':$('#id').val(),
            'name1':$('input[id=name1]').val(),
            'code1':$('input[id=code1]').val(),             
    },

        success:function(data){
            console.log('data', data);
                       
            if(data.status=='ok'){
                 window.location.reload(true);


            }


        },
        error:function(xhr, errmsg,err){
            console.log('error', xhr);
            console.log('status', errmsg);
            console.log('err', err);

        }

    });
});



$('.deleteCurrency').click(function(){
    console.log('this');
    let id= $(this).prop('id');
    var delId= id.substring(3, 5);
    var rowId= "#tr" + delId;
    console.log( rowId);
   
    $.ajax({

    type: 'POST',
    url: '/deleteCurrency',
    
    data:{

        delId,
        "_token": $('#token').val()
    },
    dataType:"json",

    success:function(data){
        console.log('success');
        if (data.status=='ok'){
            console.log("datastatus");

             $( rowId ).remove();

      }
    },
     error:function(xhr,errmsg,err){
        console.log('error', xhr);
        console.log('status', errmsg);
        console.log('err', err);
    }


});

});



$('.statusSubmitButton').click(function(e){
    e.preventDefault();
    console.log("this")

$.ajax({

    type: 'POST',
    url: '/addStatus',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data:{
        'status': $('input[id= status]').val(),
    },
    dataType:"json",

    success:function(data){
        if (data.status=='ok'){

            window.location.reload(true);
        }
    }


});

});

function editStatus(status,id){
    $('#status1').val(status);
    $('#id').val(id);

}
$('.editStatusButton').click(function(e){
    e.preventDefault();
    console.log('This');
    let id=$(this).prop('id');
    console.log(id);

    $.ajax({
        type:'POST',
        url:'/editStatus',
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
        data:{
            'id':$('#id').val(),
            'status1':$('input[id=status1]').val(),
                         
    },

        success:function(data){
            console.log('data', data);
                       
            if(data.status=='ok'){
                 window.location.reload(true);


            }


        },
        error:function(xhr, errmsg,err){
            console.log('error', xhr);
            console.log('status', errmsg);
            console.log('err', err);

        }

    });
});

$('.deleteStatus').click(function(){
    console.log('this');
    let id= $(this).prop('id');
    var delId= id.substring(3, 5);
    var rowId= "#tr" + delId;
    console.log( rowId);
   
    $.ajax({

    type: 'POST',
    url: '/deleteStatus',
    
    data:{

        delId,
        "_token": $('#token').val()
    },
    dataType:"json",

    success:function(data){
        console.log('success');
        if (data.status=='ok'){
            console.log("datastatus");

             $( rowId ).remove();

      }
    },
     error:function(xhr,errmsg,err){
        console.log('error', xhr);
        console.log('status', errmsg);
        console.log('err', err);
    }


});

});


$('.paymentSubmitButton').click(function(e){
    e.preventDefault();
    console.log("this")

$.ajax({

    type: 'POST',
    url: '/addPayment',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data:{
        'payment': $('input[id= payment]').val(),
    },
    dataType:"json",

    success:function(data){
        if (data.status=='ok'){

            window.location.reload(true);
        }
    }


});

});

function editPayment(payment,id){
    $('#payment1').val(payment);
    $('#id').val(id);

}
$('.editPaymentButton').click(function(e){
    e.preventDefault();
    console.log('This');
    let id=$(this).prop('id');
    console.log(id);

    $.ajax({
        type:'POST',
        url:'/editPayment',
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
        data:{
            'id':$('#id').val(),
            'payment1':$('input[id=payment1]').val(),
                         
    },

        success:function(data){
            console.log('data', data);
                       
            if(data.status=='ok'){
                 window.location.reload(true);


            }


        },
        error:function(xhr, errmsg,err){
            console.log('error', xhr);
            console.log('status', errmsg);
            console.log('err', err);

        }

    });
});

$('.deletePayment').click(function(){
    console.log('this');
    let id= $(this).prop('id');
    var delId= id.substring(3, 5);
    var rowId= "#tr" + delId;
    console.log( rowId);
   
    $.ajax({

    type: 'POST',
    url: '/deletePayment',
    
    data:{

        delId,
        "_token": $('#token').val()
    },
    dataType:"json",

    success:function(data){
        console.log('success');
        if (data.status=='ok'){
            console.log("datastatus");

             $( rowId ).remove();

      }
    },
     error:function(xhr,errmsg,err){
        console.log('error', xhr);
        console.log('status', errmsg);
        console.log('err', err);
    }


});

});


